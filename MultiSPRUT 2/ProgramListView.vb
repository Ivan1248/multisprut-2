﻿Public Class ProgramListView : Inherits ListView

    Enum RedniBrojParametra As Integer : FormatUlaza = 1 : FormatIzlaza = 2 : PutanjaPrograma = 3 : End Enum

    Public Property Stavka(indeks As Integer, redniBroj As RedniBrojParametra) As String
        Get
            Return Me.Items(indeks).SubItems(redniBroj).Text
        End Get
        Set
            Me.Items(indeks).SubItems(redniBroj).Text = Value
        End Set
    End Property

    Enum ItemMoveDirection : Up : Down : End Enum

    Public Sub MoveSelecteditem(direction As ItemMoveDirection)
        If Me.SelectedIndices.Count = 0 Then Exit Sub
        Dim i As Integer = Me.SelectedIndices(0)
        Select Case direction
            Case ItemMoveDirection.Up
                If i = 0 Then Exit Sub
                Dim stavka As ListViewItem = Me.SelectedItems(0)
                Me.Items.RemoveAt(i)
                Me.Items.Insert(i - 1, stavka)
            Case ItemMoveDirection.Down
                If i = Me.Items.Count - 1 Then Exit Sub
                Dim stavka As ListViewItem = Me.SelectedItems(0)
                Me.Items.RemoveAt(i)
                Me.Items.Insert(i + 1, stavka)
        End Select
    End Sub
End Class
