﻿Imports System.IO
Imports System.Runtime.InteropServices
Imports System.Text
Imports System.Threading

Public Class Form1
    Private _radniDirektorij As String
    Private _direktorijZaGreske As String
    Private _testovi As New List(Of Test)

    Private ReadOnly Property KonfiguracijskaDatoteka
        Get
            Return Path.Combine(_radniDirektorij, "MultiSPRUT.cfg")
        End Get
    End Property

    Private Sub OdaberiMapuButton_Click(sender As Object, e As EventArgs) Handles OdaberiMapuButton.Click
        If FolderBrowserDialogmapa.ShowDialog() = DialogResult.OK Then
            MapaTB.Text = FolderBrowserDialogmapa.SelectedPath
            _radniDirektorij = MapaTB.Text
            _direktorijZaGreske = Path.Combine(_radniDirektorij, "Neispravni izlazi")
            UcitajProgrameITestove()
            Panel1.Enabled = True
        End If
    End Sub

    Private Sub OsvjeziButton_Click(sender As Object, e As EventArgs) Handles OsvjeziButton.Click
        UcitajProgrameITestove()
    End Sub

    Private Sub UcitajProgrameITestove()
        UcitajPrograme()
        UcitajTestove()
    End Sub

    Dim _programi As New List(Of TestiraniProgram)

    Sub UcitajPrograme()
        _programi.Clear()
        UcitajProgrameIzKonfiguracijskeDatoteke()
        PronadjiNovePrograme(_radniDirektorij)

        IzmjenaFlowLayoutPanel.Enabled = False
        ListView1.Items.Clear()
        For Each p As TestiraniProgram In _programi
            Dim s As New ListViewItem(p.Ime)
            With s.SubItems
                .Add(p.FormatUlaza)
                .Add(p.FormatIzlaza)
                .Add(p.Putanja)
            End With
            ListView1.Items.Add(s)
        Next
    End Sub

    Private Sub UcitajProgrameIzKonfiguracijskeDatoteke()
        If Not File.Exists(KonfiguracijskaDatoteka) Then Exit Sub
        Using fs As FileStream = File.OpenRead(KonfiguracijskaDatoteka)
            Dim sr As New StreamReader(fs)
            Dim pr As New TestiraniProgram
            While sr.Peek() <> -1
                Dim line As String = sr.ReadLine()
                If line.Length = 0 Then
                    If (File.Exists(pr.Putanja)) Then _programi.Add(pr)
                ElseIf line.StartsWith("\"c) Then
                    pr = New TestiraniProgram()
                    pr.Ime = line
                    pr.Putanja = Path.Combine(_radniDirektorij, line.Substring(1))
                ElseIf line.StartsWith("<"c) Then
                    pr.FormatUlaza = line.Substring(2)
                ElseIf line.StartsWith(">"c) Then
                    pr.FormatIzlaza = line.Substring(2)
                End If
            End While
        End Using
    End Sub

    Sub PronadjiNovePrograme(pocetniDirektorij As String)
        Dim exeFiles As String() = Directory.GetFiles(pocetniDirektorij, My.Resources.ExecutableFormat)
        For Each f In exeFiles
            If _programi.Exists(Function(p) (p.Putanja = f)) Then Continue For
            Dim pui As New TestiraniProgram
            pui.Putanja = f
            pui.Ime = f.Substring(_radniDirektorij.Length, f.Length - _radniDirektorij.Length)
            _programi.Add(pui)
        Next
        For Each dir As String In Directory.GetDirectories(pocetniDirektorij)
            PronadjiNovePrograme(dir)
        Next
    End Sub

    Sub UcitajTestove()
        _testovi.Clear()
        Dim mapa As String = Path.Combine(_radniDirektorij, "Testovi")
        If Not Directory.Exists(mapa) Then
            MsgBox("MultiSPRUT ne može pronaći mapu s testovima " & mapa & ".")
            Exit Sub
        End If

        Dim td As String() = Directory.GetDirectories(mapa)
        For Each d As String In td
            Try
                Dim tp As New Test
                tp.Ime = Path.GetFileName(d)
                tp.Datoteke = Directory.GetFiles(d)
                _testovi.Add(tp)
            Catch
            End Try
        Next

        Dim usporedba As New Comparison(Of Test)(
            Function(t1, t2)
                Dim s1 = t1.Ime, s2 = t2.Ime
                Dim d As Integer = s1.Length - s2.Length
                If d <> 0 Then Return d
                For i = 0 To s1.Length - 1
                    d = Asc(s1(i)) - Asc(s2(i))
                    If d <> 0 Then Return d
                Next
                Return 0
            End Function)

        _testovi.Sort(usporedba)
        InformacijeOTestovimaLabel.Text = "Broj testova: " & td.Length.ToString()
    End Sub

    Private Sub SpremiButton_Click(sender As Object, e As EventArgs) Handles SpremiButton.Click
        PripremiZaIspitivanjeISpremi()
    End Sub

    Sub PripremiZaIspitivanjeISpremi()
        Dim sb As New StringBuilder()
        _programi.Clear()
        For i As Integer = 0 To ListView1.Items.Count - 1
            _programi.Add(New TestiraniProgram(ListView1.Items(i).Text,
                Path.Combine(_radniDirektorij, ListView1.Items(i).Text.Substring(1)),
                ListView1.Stavka(i, ProgramListView.RedniBrojParametra.FormatUlaza),
                ListView1.Stavka(i, ProgramListView.RedniBrojParametra.FormatIzlaza)))
        Next
        For Each pr As TestiraniProgram In _programi
            sb.AppendLine(pr.Ime)
            sb.Append("< ").AppendLine(pr.FormatUlaza)
            sb.Append("> ").AppendLine(pr.FormatIzlaza)
            sb.AppendLine()
        Next
        File.WriteAllText(KonfiguracijskaDatoteka, sb.ToString())
    End Sub

    Private Sub PomakniGoreButton_Click(sender As Object, e As EventArgs) Handles PomakniGoreButton.Click
        ListView1.MoveSelecteditem(ProgramListView.ItemMoveDirection.Up)
    End Sub

    Private Sub PomakniDoljeButton_Click(sender As Object, e As EventArgs) Handles PomakniDoljeButton.Click
        ListView1.MoveSelecteditem(ProgramListView.ItemMoveDirection.Down)
    End Sub

    Private Sub ListView1_ItemSelectionChanged(sender As Object, e As ListViewItemSelectionChangedEventArgs) Handles ListView1.ItemSelectionChanged
        IzmjenaFlowLayoutPanel.Enabled = e.IsSelected
        If e.IsSelected Then
            FormatUlazTB.Text = e.Item.SubItems(ProgramListView.RedniBrojParametra.FormatUlaza).Text
            FormatIzlazTB.Text = e.Item.SubItems(ProgramListView.RedniBrojParametra.FormatIzlaza).Text
        Else
            FormatUlazTB.Text = String.Empty
            FormatIzlazTB.Text = String.Empty
        End If
    End Sub

    Private Sub PreusUlazTB_TextChanged(sender As Object, e As EventArgs) Handles FormatUlazTB.TextChanged
        If ListView1.SelectedItems.Count > 0 Then
            Dim stavka As ListViewItem = ListView1.SelectedItems(0)
            stavka.SubItems(ProgramListView.RedniBrojParametra.FormatUlaza).Text = FormatUlazTB.Text
        End If
    End Sub

    Private Sub PreusIzlazTB_TextChanged(sender As Object, e As EventArgs) Handles FormatIzlazTB.TextChanged
        If ListView1.SelectedItems.Count > 0 Then
            Dim stavka As ListViewItem = ListView1.SelectedItems(0)
            stavka.SubItems(ProgramListView.RedniBrojParametra.FormatIzlaza).Text = FormatIzlazTB.Text
        End If
    End Sub

    Private Sub IspitajButton_Click(sender As Object, e As EventArgs) Handles IspitajButton.Click
        If _programi.Count = 0 Then Exit Sub

        If IspitajButton.Text = "Ispitaj" Then
            PripremiZaIspitivanjeISpremi()
            StvoriIliOcistiDirektorijZaGreske()
            ToolStripProgressBar1.Maximum = _testovi.Count
            ToolStripProgressBar1.Value = 0
            Dim t As Task = Task.Factory.StartNew(AddressOf IspitajSve)
            IspitajButton.Text = "Zaustavi"
        Else
            cts.Cancel()
            IspitajButton.Text = "Ispitaj"
            ObaviRadnjePoslijeIspitivanja()
        End If
    End Sub

    Dim cts As CancellationTokenSource

    Private Sub IspitajSve()
        Dim ispitano As Integer = 0
        Dim neispravno As Integer = 0

        cts = New CancellationTokenSource
        Dim po As New ParallelOptions
        po.CancellationToken = cts.Token
        po.MaxDegreeOfParallelism = System.Environment.ProcessorCount
        Try
            Parallel.ForEach(_testovi, po,
                Sub(test)
                    If Not Ispitaj(test) Then Interlocked.Increment(neispravno)
                    Interlocked.Increment(ispitano)
                    BeginInvoke(Sub() Me.AzurirajPrikazNapretka(ispitano, neispravno))
                End Sub)
        Finally
            cts.Dispose()
        End Try
        BeginInvoke(Sub() ObaviRadnjePoslijeIspitivanja())
    End Sub

    Private Sub AzurirajPrikazNapretka(ispitano As Integer, neispravno As Integer)
        ToolStripProgressBar1.Value = ispitano
        ToolStripStatusLabel1.Text = (ispitano - neispravno) & "/"c & ispitano & " ispravno"
    End Sub

    Private Sub ObaviRadnjePoslijeIspitivanja()
        OtvoriMapuUExploreru(_direktorijZaGreske)
        IspitajButton.Text = "Ispitaj"
    End Sub

    Function Ispitaj(test As Test) As Boolean
        For Each p As TestiraniProgram In _programi
            Dim psi As New ProcessStartInfo(p.Putanja)
            With psi
                .WorkingDirectory = _radniDirektorij
                .CreateNoWindow = True
                .UseShellExecute = False
                .RedirectStandardInput = Not String.IsNullOrEmpty(p.FormatUlaza)
                .RedirectStandardOutput = Not String.IsNullOrEmpty(p.FormatIzlaza)
            End With

            Dim pr As Process = Process.Start(psi)
            pr.StandardInput.AutoFlush = True
            If Not String.IsNullOrEmpty(p.FormatUlaza) Then
                Dim ulaz As String = File.ReadAllText(test.Datoteke.First(Function(d) d Like p.FormatUlaza))
                pr.StandardInput.WriteLine(ulaz)
                'pr.StandardInput.Close()
            End If

            If String.IsNullOrEmpty(p.FormatIzlaza) Then GoTo cont

            Dim putanja = Path.Combine(_direktorijZaGreske, test.Ime & Path.GetExtension(p.FormatIzlaza))
            Dim ocekivaniIzlaz = File.ReadAllText(test.Datoteke.First(Function(d) d Like p.FormatIzlaza)).Replace(vbCrLf, vbLf)
            Dim izlaz As String = String.Empty
            Dim timeout As Integer = NumericUpDown1.Value
            Dim sw As New Stopwatch()
            sw.Start()
            While pr.StandardOutput.Peek() > -1
                While pr.StandardOutput.Peek() = 0
                    Thread.Sleep(1)
                    If sw.ElapsedMilliseconds > timeout Then
                        pr.Kill()
                        File.WriteAllText(putanja,
                            "Program nije završio s radom prije isteka " & timeout.ToString() & "ms." & vbLf & izlaz)
                        Return False
                    End If
                End While
                izlaz &= pr.StandardOutput.ReadLine() & vbLf
            End While

            If izlaz <> ocekivaniIzlaz Then
                File.WriteAllText(putanja, izlaz)
                'test.Rezultat = New RezultatTesta(ocekivaniIzlaz, izlaz)
                Return False
            Else
                'test.Rezultat = Nothing
            End If

cont:       pr.WaitForExit()
        Next
        Return True
    End Function


    Private Sub StvoriIliOcistiDirektorijZaGreske()
        If Directory.Exists(_direktorijZaGreske) Then
            For Each f As String In Directory.GetFiles(_direktorijZaGreske)
                File.Delete(f)
            Next
        Else
            Directory.CreateDirectory(_direktorijZaGreske)
        End If
    End Sub

    <DllImport("user32.dll", SetLastError:=True, CharSet:=CharSet.Auto)>
    Private Shared Function FindWindow(ByVal lpClassName As String, ByVal lpWindowName As String) As IntPtr
    End Function

    Sub OtvoriMapuUExploreru(path As String)
        Dim name As String = IO.Path.GetFileName(path)
        If FindWindow(vbNullString, name) = CType(0, IntPtr) Then
            Process.Start("explorer.exe", path)
        Else
            AppActivate(name)
            SendKeys.SendWait("~")
        End If
    End Sub

End Class
