﻿<Global.Microsoft.VisualBasic.CompilerServices.DesignerGenerated()>
Partial Class Form1
    Inherits System.Windows.Forms.Form

    'Form overrides dispose to clean up the component list.
    <System.Diagnostics.DebuggerNonUserCode()>
    Protected Overrides Sub Dispose(ByVal disposing As Boolean)
        Try
            If disposing AndAlso components IsNot Nothing Then
                components.Dispose()
            End If
        Finally
            MyBase.Dispose(disposing)
        End Try
    End Sub

    'Required by the Windows Form Designer
    Private components As System.ComponentModel.IContainer

    'NOTE: The following procedure is required by the Windows Form Designer
    'It can be modified using the Windows Form Designer.  
    'Do not modify it using the code editor.
    <System.Diagnostics.DebuggerStepThrough()>
    Private Sub InitializeComponent()
        Me.FlowLayoutPanel1 = New System.Windows.Forms.FlowLayoutPanel()
        Me.GroupBox2 = New System.Windows.Forms.GroupBox()
        Me.OdaberiMapuButton = New System.Windows.Forms.Button()
        Me.MapaTB = New System.Windows.Forms.TextBox()
        Me.GroupBox1 = New System.Windows.Forms.GroupBox()
        Me.InformacijeOTestovimaLabel = New System.Windows.Forms.Label()
        Me.FlowLayoutPanel2 = New System.Windows.Forms.FlowLayoutPanel()
        Me.Panel1 = New System.Windows.Forms.Panel()
        Me.OsvjeziButton = New System.Windows.Forms.Button()
        Me.PomakniDoljeButton = New System.Windows.Forms.Button()
        Me.PomakniGoreButton = New System.Windows.Forms.Button()
        Me.SpremiButton = New System.Windows.Forms.Button()
        Me.IzmjenaFlowLayoutPanel = New System.Windows.Forms.FlowLayoutPanel()
        Me.Izmjenalabel = New System.Windows.Forms.Label()
        Me.FormatUlazTB = New System.Windows.Forms.TextBox()
        Me.FormatIzlazTB = New System.Windows.Forms.TextBox()
        Me.GroupBox4 = New System.Windows.Forms.GroupBox()
        Me.FlowLayoutPanel3 = New System.Windows.Forms.FlowLayoutPanel()
        Me.Label1 = New System.Windows.Forms.Label()
        Me.NumericUpDown1 = New System.Windows.Forms.NumericUpDown()
        Me.IspitajButton = New System.Windows.Forms.Button()
        Me.StatusStrip1 = New System.Windows.Forms.StatusStrip()
        Me.ToolStripProgressBar1 = New System.Windows.Forms.ToolStripProgressBar()
        Me.ToolStripStatusLabel1 = New System.Windows.Forms.ToolStripStatusLabel()
        Me.FolderBrowserDialogIzvorniKod = New System.Windows.Forms.FolderBrowserDialog()
        Me.FolderBrowserDialogmapa = New System.Windows.Forms.FolderBrowserDialog()
        Me.OpenFileDialog1 = New System.Windows.Forms.OpenFileDialog()
        Me.ListView1 = New MultiSPRUT_2.ProgramListView()
        Me.Program = CType(New System.Windows.Forms.ColumnHeader(),System.Windows.Forms.ColumnHeader)
        Me.ImeUlazneDatoteke = CType(New System.Windows.Forms.ColumnHeader(),System.Windows.Forms.ColumnHeader)
        Me.ImeIzlazneDatoteke = CType(New System.Windows.Forms.ColumnHeader(),System.Windows.Forms.ColumnHeader)
        Me.PutanjaPrograma = CType(New System.Windows.Forms.ColumnHeader(),System.Windows.Forms.ColumnHeader)
        Me.FlowLayoutPanel1.SuspendLayout
        Me.GroupBox2.SuspendLayout
        Me.GroupBox1.SuspendLayout
        Me.FlowLayoutPanel2.SuspendLayout
        Me.Panel1.SuspendLayout
        Me.IzmjenaFlowLayoutPanel.SuspendLayout
        Me.GroupBox4.SuspendLayout
        Me.FlowLayoutPanel3.SuspendLayout
        CType(Me.NumericUpDown1,System.ComponentModel.ISupportInitialize).BeginInit
        Me.StatusStrip1.SuspendLayout
        Me.SuspendLayout
        '
        'FlowLayoutPanel1
        '
        Me.FlowLayoutPanel1.AutoSize = true
        Me.FlowLayoutPanel1.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.FlowLayoutPanel1.Controls.Add(Me.GroupBox2)
        Me.FlowLayoutPanel1.Controls.Add(Me.GroupBox1)
        Me.FlowLayoutPanel1.Controls.Add(Me.GroupBox4)
        Me.FlowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill
        Me.FlowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.TopDown
        Me.FlowLayoutPanel1.Location = New System.Drawing.Point(0, 0)
        Me.FlowLayoutPanel1.Name = "FlowLayoutPanel1"
        Me.FlowLayoutPanel1.Padding = New System.Windows.Forms.Padding(4)
        Me.FlowLayoutPanel1.Size = New System.Drawing.Size(403, 394)
        Me.FlowLayoutPanel1.TabIndex = 0
        '
        'GroupBox2
        '
        Me.GroupBox2.AutoSize = true
        Me.GroupBox2.Controls.Add(Me.OdaberiMapuButton)
        Me.GroupBox2.Controls.Add(Me.MapaTB)
        Me.GroupBox2.Location = New System.Drawing.Point(7, 7)
        Me.GroupBox2.Name = "GroupBox2"
        Me.GroupBox2.Padding = New System.Windows.Forms.Padding(5)
        Me.GroupBox2.Size = New System.Drawing.Size(388, 65)
        Me.GroupBox2.TabIndex = 2
        Me.GroupBox2.TabStop = false
        Me.GroupBox2.Text = "Radni direktorij"
        '
        'OdaberiMapuButton
        '
        Me.OdaberiMapuButton.Location = New System.Drawing.Point(320, 21)
        Me.OdaberiMapuButton.Name = "OdaberiMapuButton"
        Me.OdaberiMapuButton.Size = New System.Drawing.Size(60, 23)
        Me.OdaberiMapuButton.TabIndex = 1
        Me.OdaberiMapuButton.Text = "Odaberi"
        Me.OdaberiMapuButton.UseVisualStyleBackColor = true
        '
        'MapaTB
        '
        Me.MapaTB.Location = New System.Drawing.Point(8, 21)
        Me.MapaTB.Name = "MapaTB"
        Me.MapaTB.ReadOnly = true
        Me.MapaTB.Size = New System.Drawing.Size(306, 20)
        Me.MapaTB.TabIndex = 0
        '
        'GroupBox1
        '
        Me.GroupBox1.AutoSize = true
        Me.GroupBox1.Controls.Add(Me.InformacijeOTestovimaLabel)
        Me.GroupBox1.Controls.Add(Me.FlowLayoutPanel2)
        Me.GroupBox1.Controls.Add(Me.IzmjenaFlowLayoutPanel)
        Me.GroupBox1.Location = New System.Drawing.Point(7, 78)
        Me.GroupBox1.Name = "GroupBox1"
        Me.GroupBox1.Padding = New System.Windows.Forms.Padding(5)
        Me.GroupBox1.Size = New System.Drawing.Size(391, 193)
        Me.GroupBox1.TabIndex = 0
        Me.GroupBox1.TabStop = false
        Me.GroupBox1.Text = "Konfiguracija"
        '
        'InformacijeOTestovimaLabel
        '
        Me.InformacijeOTestovimaLabel.AutoSize = true
        Me.InformacijeOTestovimaLabel.Location = New System.Drawing.Point(12, 156)
        Me.InformacijeOTestovimaLabel.Name = "InformacijeOTestovimaLabel"
        Me.InformacijeOTestovimaLabel.Padding = New System.Windows.Forms.Padding(0, 3, 0, 3)
        Me.InformacijeOTestovimaLabel.Size = New System.Drawing.Size(10, 19)
        Me.InformacijeOTestovimaLabel.TabIndex = 3
        Me.InformacijeOTestovimaLabel.Text = " "
        '
        'FlowLayoutPanel2
        '
        Me.FlowLayoutPanel2.AutoSize = true
        Me.FlowLayoutPanel2.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.FlowLayoutPanel2.Controls.Add(Me.ListView1)
        Me.FlowLayoutPanel2.Controls.Add(Me.Panel1)
        Me.FlowLayoutPanel2.Location = New System.Drawing.Point(8, 18)
        Me.FlowLayoutPanel2.Margin = New System.Windows.Forms.Padding(0)
        Me.FlowLayoutPanel2.Name = "FlowLayoutPanel2"
        Me.FlowLayoutPanel2.Size = New System.Drawing.Size(378, 106)
        Me.FlowLayoutPanel2.TabIndex = 6
        '
        'Panel1
        '
        Me.Panel1.Controls.Add(Me.OsvjeziButton)
        Me.Panel1.Controls.Add(Me.PomakniDoljeButton)
        Me.Panel1.Controls.Add(Me.PomakniGoreButton)
        Me.Panel1.Controls.Add(Me.SpremiButton)
        Me.Panel1.Enabled = false
        Me.Panel1.Location = New System.Drawing.Point(315, 3)
        Me.Panel1.Name = "Panel1"
        Me.Panel1.Size = New System.Drawing.Size(60, 100)
        Me.Panel1.TabIndex = 8
        '
        'OsvjeziButton
        '
        Me.OsvjeziButton.Location = New System.Drawing.Point(0, 0)
        Me.OsvjeziButton.Name = "OsvjeziButton"
        Me.OsvjeziButton.Size = New System.Drawing.Size(60, 23)
        Me.OsvjeziButton.TabIndex = 3
        Me.OsvjeziButton.Text = "Osvježi"
        Me.OsvjeziButton.UseVisualStyleBackColor = true
        '
        'PomakniDoljeButton
        '
        Me.PomakniDoljeButton.Location = New System.Drawing.Point(32, 29)
        Me.PomakniDoljeButton.Name = "PomakniDoljeButton"
        Me.PomakniDoljeButton.Size = New System.Drawing.Size(28, 23)
        Me.PomakniDoljeButton.TabIndex = 7
        Me.PomakniDoljeButton.Text = "↓"
        Me.PomakniDoljeButton.UseVisualStyleBackColor = true
        '
        'PomakniGoreButton
        '
        Me.PomakniGoreButton.Location = New System.Drawing.Point(0, 29)
        Me.PomakniGoreButton.Name = "PomakniGoreButton"
        Me.PomakniGoreButton.Size = New System.Drawing.Size(28, 23)
        Me.PomakniGoreButton.TabIndex = 6
        Me.PomakniGoreButton.Text = "↑"
        Me.PomakniGoreButton.UseVisualStyleBackColor = true
        '
        'SpremiButton
        '
        Me.SpremiButton.Location = New System.Drawing.Point(0, 58)
        Me.SpremiButton.Name = "SpremiButton"
        Me.SpremiButton.Size = New System.Drawing.Size(60, 23)
        Me.SpremiButton.TabIndex = 4
        Me.SpremiButton.Text = "Spremi"
        Me.SpremiButton.UseVisualStyleBackColor = true
        '
        'IzmjenaFlowLayoutPanel
        '
        Me.IzmjenaFlowLayoutPanel.AutoSize = true
        Me.IzmjenaFlowLayoutPanel.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.IzmjenaFlowLayoutPanel.Controls.Add(Me.Izmjenalabel)
        Me.IzmjenaFlowLayoutPanel.Controls.Add(Me.FormatUlazTB)
        Me.IzmjenaFlowLayoutPanel.Controls.Add(Me.FormatIzlazTB)
        Me.IzmjenaFlowLayoutPanel.Enabled = false
        Me.IzmjenaFlowLayoutPanel.Location = New System.Drawing.Point(8, 127)
        Me.IzmjenaFlowLayoutPanel.Margin = New System.Windows.Forms.Padding(3, 0, 3, 3)
        Me.IzmjenaFlowLayoutPanel.Name = "IzmjenaFlowLayoutPanel"
        Me.IzmjenaFlowLayoutPanel.Size = New System.Drawing.Size(306, 26)
        Me.IzmjenaFlowLayoutPanel.TabIndex = 5
        '
        'Izmjenalabel
        '
        Me.Izmjenalabel.Location = New System.Drawing.Point(4, 3)
        Me.Izmjenalabel.Margin = New System.Windows.Forms.Padding(4, 3, 0, 3)
        Me.Izmjenalabel.Name = "Izmjenalabel"
        Me.Izmjenalabel.Padding = New System.Windows.Forms.Padding(0, 4, 0, 0)
        Me.Izmjenalabel.Size = New System.Drawing.Size(102, 20)
        Me.Izmjenalabel.TabIndex = 0
        Me.Izmjenalabel.Text = "Izmjena odabranog:"
        '
        'FormatUlazTB
        '
        Me.FormatUlazTB.Location = New System.Drawing.Point(109, 3)
        Me.FormatUlazTB.MaxLength = 31
        Me.FormatUlazTB.Name = "FormatUlazTB"
        Me.FormatUlazTB.Size = New System.Drawing.Size(94, 20)
        Me.FormatUlazTB.TabIndex = 5
        Me.FormatUlazTB.Text = "*.ulaz"
        '
        'FormatIzlazTB
        '
        Me.FormatIzlazTB.Location = New System.Drawing.Point(209, 3)
        Me.FormatIzlazTB.MaxLength = 31
        Me.FormatIzlazTB.Name = "FormatIzlazTB"
        Me.FormatIzlazTB.Size = New System.Drawing.Size(94, 20)
        Me.FormatIzlazTB.TabIndex = 5
        Me.FormatIzlazTB.Text = "*.izlaz"
        '
        'GroupBox4
        '
        Me.GroupBox4.AutoSize = true
        Me.GroupBox4.Controls.Add(Me.FlowLayoutPanel3)
        Me.GroupBox4.Controls.Add(Me.IspitajButton)
        Me.GroupBox4.Location = New System.Drawing.Point(7, 277)
        Me.GroupBox4.Name = "GroupBox4"
        Me.GroupBox4.Padding = New System.Windows.Forms.Padding(5)
        Me.GroupBox4.Size = New System.Drawing.Size(388, 65)
        Me.GroupBox4.TabIndex = 4
        Me.GroupBox4.TabStop = false
        Me.GroupBox4.Text = "Ispitivanje"
        '
        'FlowLayoutPanel3
        '
        Me.FlowLayoutPanel3.AutoSize = true
        Me.FlowLayoutPanel3.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.FlowLayoutPanel3.Controls.Add(Me.Label1)
        Me.FlowLayoutPanel3.Controls.Add(Me.NumericUpDown1)
        Me.FlowLayoutPanel3.Location = New System.Drawing.Point(8, 18)
        Me.FlowLayoutPanel3.Margin = New System.Windows.Forms.Padding(3, 0, 3, 3)
        Me.FlowLayoutPanel3.Name = "FlowLayoutPanel3"
        Me.FlowLayoutPanel3.Size = New System.Drawing.Size(288, 26)
        Me.FlowLayoutPanel3.TabIndex = 7
        '
        'Label1
        '
        Me.Label1.AutoSize = true
        Me.Label1.Location = New System.Drawing.Point(4, 3)
        Me.Label1.Margin = New System.Windows.Forms.Padding(4, 3, 0, 3)
        Me.Label1.Name = "Label1"
        Me.Label1.Padding = New System.Windows.Forms.Padding(0, 4, 0, 0)
        Me.Label1.Size = New System.Drawing.Size(218, 17)
        Me.Label1.TabIndex = 0
        Me.Label1.Text = "Maks. vrijeme izvođenja pojedinog testa[ms]:"
        '
        'NumericUpDown1
        '
        Me.NumericUpDown1.Location = New System.Drawing.Point(225, 3)
        Me.NumericUpDown1.Maximum = New Decimal(New Integer() {800000, 0, 0, 0})
        Me.NumericUpDown1.Name = "NumericUpDown1"
        Me.NumericUpDown1.Size = New System.Drawing.Size(60, 20)
        Me.NumericUpDown1.TabIndex = 7
        Me.NumericUpDown1.Value = New Decimal(New Integer() {30000, 0, 0, 0})
        '
        'IspitajButton
        '
        Me.IspitajButton.Location = New System.Drawing.Point(320, 21)
        Me.IspitajButton.Name = "IspitajButton"
        Me.IspitajButton.Size = New System.Drawing.Size(60, 23)
        Me.IspitajButton.TabIndex = 2
        Me.IspitajButton.Text = "Ispitaj"
        Me.IspitajButton.UseVisualStyleBackColor = true
        '
        'StatusStrip1
        '
        Me.StatusStrip1.Items.AddRange(New System.Windows.Forms.ToolStripItem() {Me.ToolStripProgressBar1, Me.ToolStripStatusLabel1})
        Me.StatusStrip1.Location = New System.Drawing.Point(0, 394)
        Me.StatusStrip1.Name = "StatusStrip1"
        Me.StatusStrip1.Size = New System.Drawing.Size(403, 22)
        Me.StatusStrip1.TabIndex = 1
        Me.StatusStrip1.Text = "StatusStrip1"
        '
        'ToolStripProgressBar1
        '
        Me.ToolStripProgressBar1.Name = "ToolStripProgressBar1"
        Me.ToolStripProgressBar1.Size = New System.Drawing.Size(100, 16)
        '
        'ToolStripStatusLabel1
        '
        Me.ToolStripStatusLabel1.Name = "ToolStripStatusLabel1"
        Me.ToolStripStatusLabel1.Size = New System.Drawing.Size(55, 17)
        Me.ToolStripStatusLabel1.Text = "Spremno"
        '
        'OpenFileDialog1
        '
        Me.OpenFileDialog1.FileName = "OpenFileDialog1"
        '
        'ListView1
        '
        Me.ListView1.Columns.AddRange(New System.Windows.Forms.ColumnHeader() {Me.Program, Me.ImeUlazneDatoteke, Me.ImeIzlazneDatoteke, Me.PutanjaPrograma})
        Me.ListView1.HideSelection = false
        Me.ListView1.Location = New System.Drawing.Point(3, 3)
        Me.ListView1.MultiSelect = false
        Me.ListView1.Name = "ListView1"
        Me.ListView1.Size = New System.Drawing.Size(306, 100)
        Me.ListView1.TabIndex = 2
        Me.ListView1.UseCompatibleStateImageBehavior = false
        Me.ListView1.View = System.Windows.Forms.View.Details
        '
        'Program
        '
        Me.Program.Text = "Program"
        Me.Program.Width = 140
        '
        'ImeUlazneDatoteke
        '
        Me.ImeUlazneDatoteke.Text = "Imena ulaza"
        Me.ImeUlazneDatoteke.Width = 80
        '
        'ImeIzlazneDatoteke
        '
        Me.ImeIzlazneDatoteke.Text = "Imena izlaza"
        Me.ImeIzlazneDatoteke.Width = 80
        '
        'PutanjaPrograma
        '
        Me.PutanjaPrograma.Text = "Putanja programa"
        Me.PutanjaPrograma.Width = 2
        '
        'Form1
        '
        Me.AutoScaleDimensions = New System.Drawing.SizeF(6!, 13!)
        Me.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font
        Me.AutoSize = true
        Me.AutoSizeMode = System.Windows.Forms.AutoSizeMode.GrowAndShrink
        Me.ClientSize = New System.Drawing.Size(403, 416)
        Me.Controls.Add(Me.FlowLayoutPanel1)
        Me.Controls.Add(Me.StatusStrip1)
        Me.Name = "Form1"
        Me.Text = "MutliSPRUT 2"
        Me.FlowLayoutPanel1.ResumeLayout(false)
        Me.FlowLayoutPanel1.PerformLayout
        Me.GroupBox2.ResumeLayout(false)
        Me.GroupBox2.PerformLayout
        Me.GroupBox1.ResumeLayout(false)
        Me.GroupBox1.PerformLayout
        Me.FlowLayoutPanel2.ResumeLayout(false)
        Me.Panel1.ResumeLayout(false)
        Me.IzmjenaFlowLayoutPanel.ResumeLayout(false)
        Me.IzmjenaFlowLayoutPanel.PerformLayout
        Me.GroupBox4.ResumeLayout(false)
        Me.GroupBox4.PerformLayout
        Me.FlowLayoutPanel3.ResumeLayout(false)
        Me.FlowLayoutPanel3.PerformLayout
        CType(Me.NumericUpDown1,System.ComponentModel.ISupportInitialize).EndInit
        Me.StatusStrip1.ResumeLayout(false)
        Me.StatusStrip1.PerformLayout
        Me.ResumeLayout(false)
        Me.PerformLayout

End Sub

    Friend WithEvents FlowLayoutPanel1 As FlowLayoutPanel
    Friend WithEvents StatusStrip1 As StatusStrip
    Friend WithEvents ToolStripProgressBar1 As ToolStripProgressBar
    Friend WithEvents GroupBox1 As GroupBox
    Friend WithEvents GroupBox2 As GroupBox
    Friend WithEvents OdaberiMapuButton As Button
    Friend WithEvents MapaTB As TextBox
    Friend WithEvents GroupBox4 As GroupBox
    Friend WithEvents IspitajButton As Button
    Friend WithEvents FolderBrowserDialogIzvorniKod As FolderBrowserDialog
    Friend WithEvents ListView1 As ProgramListView
    Friend WithEvents Program As ColumnHeader
    Friend WithEvents PutanjaPrograma As ColumnHeader
    Friend WithEvents ImeUlazneDatoteke As ColumnHeader
    Friend WithEvents ImeIzlazneDatoteke As ColumnHeader
    Friend WithEvents SpremiButton As Button
    Friend WithEvents OsvjeziButton As Button
    Friend WithEvents OpenFileDialog1 As OpenFileDialog
    Friend WithEvents FormatIzlazTB As TextBox
    Friend WithEvents FormatUlazTB As TextBox
    Friend WithEvents IzmjenaFlowLayoutPanel As FlowLayoutPanel
    Friend WithEvents Izmjenalabel As Label
    Friend WithEvents PomakniDoljeButton As Button
    Friend WithEvents PomakniGoreButton As Button
    Friend WithEvents FolderBrowserDialogmapa As FolderBrowserDialog
    Friend WithEvents ToolStripStatusLabel1 As ToolStripStatusLabel
    Friend WithEvents FlowLayoutPanel2 As FlowLayoutPanel
    Friend WithEvents Panel1 As Panel
    Friend WithEvents InformacijeOTestovimaLabel As Label
    Friend WithEvents FlowLayoutPanel3 As FlowLayoutPanel
    Friend WithEvents Label1 As Label
    Friend WithEvents NumericUpDown1 As NumericUpDown
End Class
